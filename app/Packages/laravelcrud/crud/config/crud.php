<?php

return [
/*
   |--------------------------------------------------------------------------
   | Menu Items
   |--------------------------------------------------------------------------
   |
   | Specify your menu items to display in the left sidebar. Each menu item
   | should have a text and and a URL. You can also specify an icon from
   | Font Awesome. A string instead of an array represents a header in sidebar
   | layout. The 'can' is a filter on Laravel's built in Gate functionality.
   |
   */
    'menu' => [
        [
            'text' => 'Pursuits',
            'route'  => 'admin.pursuits.index',
            'icon' => 'child',
        ],
        [
            'text' => 'Charities',
            'route'  => 'admin.charities.index',
            'icon' => 'heart',
        ],
        [
            'text' => 'Pursuit Participations',
            'route'  => 'admin.pursuit-participations.index',
            'icon' => 'trophy',
        ],
        [
            'text' => 'Sponsors',
            'route'  => 'admin.sponsors.index',
            'icon' => 'money',
        ],
        [
            'text' => 'Users',
            'route'  => 'admin.users.index',
            'icon' => 'users',
        ],
        [
            'text' => 'Reward Levels',
            'route'  => 'admin.reward-levels.index',
            'icon' => 'level-up',
        ],
        [
            'text' => 'Rewards',
            'route'  => 'admin.rewards.index',
            'icon' => 'level-up',
        ],
        [
            'text' => 'Gift Cards',
            'route'  => 'admin.gift-cards.index',
            'icon' => 'gift',
        ],
        [
            'text' => 'Static pages',
            'route'  => 'admin.static-pages.index',
            'icon' => 'gift',
        ],
        [

            'text' => 'Donations',
            'route'  => 'admin.donations.index',
            'icon' => 'heartbeat',
        ],
        [
            'text' => 'Templates of notification',
            'route'  => 'admin.notifications.index',
            'icon' => 'bell',
        ],
    ],
    'prefix' => 'crud',
];