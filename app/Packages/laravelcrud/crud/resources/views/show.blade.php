@extends('crud::layout')

@section('content_title')
    Preview
@endsection

@section('content_body')

    <div class="box-body table-responsive">
        @if(isset($object->translatedAttributes))
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    @foreach(config('crud.languages') as $short => $language)
                        <li @if ($loop->first) class="active" @endif>
                            <a href="#tab_{{ $short }}" data-toggle="tab" aria-expanded="false">{{ $language }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach(config('crud.languages') as $short => $language)
                        <div class="tab-pane @if ($loop->first) active @endif" id="tab_{{ $short }}">
                            <table class="table table-hover">
                                <tbody>
                                @foreach($fields as $name => $field)
                                    @if(in_array($name, $object->translatedAttributes) &&
                                    ((isset($field['show_view']) && $field['show_view'] === true) ||
                                     (!isset($field['show_view']) && $field['type'] != 'hidden')))
                                        <tr>
                                            <th>{{ $object->getFieldLabel($name) }}</th>
                                            <td width="70%">
                                                {!! $object->getTranslation($short)->{$name} !!}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    @endforeach
                </div>
                <!-- /.tab-content -->
            </div>
        @endif
        <table class="table table-hover">
            <tbody>
                @foreach($fields as $name => $field)
                    @if(isset($object->translatedAttributes) && in_array($name, $object->translatedAttributes))
                        @continue
                    @endif
                    @if((isset($field['show_view']) && $field['show_view'] === true) || (!isset($field['show_view']) && $field['type'] != 'hidden'))
                    <tr>
                        <th>{{ $object->getFieldLabel($name) }}</th>
                        <td>
                            @if(isset($field['show_relation']))
                                {!! $object->{$field['show_relation']}()->getRelated()->getRelationTable($object->{$field['show_relation']}()) !!}
                            @else
                                @if(isset($field['show_mutator']))
                                    @php
                                        $field['value'] = $object->{$field['show_mutator']}();
                                    @endphp
                                @else
                                    @php
                                        $field['value'] = $object->{$name};
                                    @endphp
                                @endif

                                @if(is_array($field['value']))
                                    <ul>
                                        @foreach($field['value'] as $item)
                                            <li>{!! $item !!}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    @if($field['type'] == 'bool')
                                        @if($field['value'] == 1)
                                            <span class="label label-success">True</span>
                                        @else
                                            <span class="label label-danger">False</span>
                                        @endif
                                    @else
                                        {!! $field['value'] !!}
                                    @endif
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        @foreach($fields as $name => $field)
        @endforeach
    </div>
    <!-- /.box-body -->
    @php
        $editable = !isset($object->editable) || (isset($object->editable) && $object->editable === true);
        $deletable = !isset($object->deletable) || (isset($object->deletable) && $object->deletable === true);
    @endphp
    <div class="box-footer">
        <form action="{{ $deletable ? route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.destroy", ['id' => $object->id]) : ''}}" method="POST" class="form-inline">
            {!! method_field('DELETE') !!}
            {!! csrf_field() !!}

            @if($editable)
                <a class="btn btn-success" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.edit", ['id' => $object->id]) }}" title="Edit">
                    <i class="fa fa-edit"></i> Edit
                </a>
            @endif

            @if($deletable)
                <a class="delete-confirm btn btn-danger" data-title="Delete" data-text="Are you sure?" title="Delete">
                    <i class="fa fa-trash"></i> Delete
                </a>
            @endif

            @if($object->buttons)
                @foreach($object->buttons as $btn)
                    @php
                        $data = [];
                        if(isset($btn['data'])) {
                            foreach ($btn['data'] as $k => $v) {
                                $data[] = "data-{$k}=\"{$v}\"";
                            }
                        }
                        $data = implode($data, ' ');
                    @endphp
                    <a class="{{ $btn['classes'] }}" href="{{ $btn['url'] }}" {!! $data !!}>
                        {!! $btn['label'] !!}
                    </a>
                @endforeach
            @endif

            <a class="btn btn-warning back" href="{{ $back_url }}">
                <i class="fa  fa-arrow-left"></i> Back
            </a>
        </form>
    </div>
@endsection