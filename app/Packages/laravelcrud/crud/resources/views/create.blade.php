@extends('crud::layout')

@section('content_title')
    Creation
@endsection

@section('content_body')
    <!-- form start -->
    <form role="form" action="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.store") }}" method="POST" enctype="multipart/form-data">
        <div class="box-body">
            @if(isset($object->translatedAttributes))
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach(config('crud.languages') as $short => $language)
                            <li @if ($loop->first) class="active" @endif>
                                <a href="#tab_{{ $short }}" data-toggle="tab" aria-expanded="false">{{ $language }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach(config('crud.languages') as $short => $language)
                            <div class="tab-pane @if ($loop->first) active @endif" id="tab_{{ $short }}">
                                @foreach($object->translatedAttributes as $name)
                                    @if(isset($fields[$name]) && View::exists('crud::fields.'.$fields[$name]['type']))
                                        @php
                                            $fields[$name]['lang'] = $short;
                                        @endphp
                                        @include('crud::fields.'.$fields[$name]['type'], $fields[$name])
                                    @endif
                                @endforeach
                            </div>
                            <!-- /.tab-pane -->
                        @endforeach
                    </div>
                    <!-- /.tab-content -->
                </div>
            @endif
            @foreach($fields as $name => $field)
                @if(isset($object->translatedAttributes) && in_array($name, $object->translatedAttributes))
                    @continue
                @endif
                @if(View::exists('crud::fields.'.$field['type']))
                    @include('crud::fields.'.$field['type'], $field)
                @endif
            @endforeach

            @if($relation)
                <div class="relations">
                    @foreach(old($relation, $object->{$relation}) as $index => $item)
                        {!! $object->{$relation}()->getRelated()->getRelationEditForm($relation, $class, $item, $index) !!}
                    @endforeach
                </div>
                <a class="add-relation" href="javascript:void(0)">Add {{ str_singular(ucfirst(str_replace('_', ' ', snake_case($relation)))) }}</a>
            @endif
        </div>
        <!-- /.box-body -->
        {!! csrf_field() !!}

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">
                <i class="fa  fa-plus"></i> Create
            </button>
            <a class="btn btn-warning back" href="{{ $back_url }}">
                <i class="fa  fa-arrow-left"></i> Back
            </a>
        </div>
    </form>
@endsection
@push('js')
    @if($relation)
        <script>
            (function(){
                'use strict';
                var i = {{ count(old($relation, $object->{$relation})) }};
                $('.add-relation').click(function(e){
                    e.preventDefault();
                    var removeRelation = '{{ str_singular(ucfirst(str_replace('_', ' ', snake_case($relation)))) }}';
                    $('.relations').append(tmpl('tmpl-create-form', {i: i, btn: removeRelation}));
                    i++;
                });
                $(document).on('click', '.remove-relation', function(e){
                    e.preventDefault();
                    $(this).parent().remove();
                });
            })();
        </script>
        <script type="text/x-tmpl" id="tmpl-create-form">
            {!! $object->{$relation}()->getRelated()->getRelationCreateForm($relation, $class) !!}
        </script>
    @endif
    {!! $js ?? '' !!}
@endpush
