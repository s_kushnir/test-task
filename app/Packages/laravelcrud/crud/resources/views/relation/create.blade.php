<div class="box-body">
    @if(isset($object->translatedAttributes))
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @foreach(config('crud.languages') as $short => $language)
                    <li @if ($loop->first) class="active" @endif>
                        <a href="#tab_{{ $short }}_{%=o.i%}" data-toggle="tab" aria-expanded="false">{{ $language }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(config('crud.languages') as $short => $language)
                    <div class="tab-pane @if ($loop->first) active @endif" id="tab_{{ $short }}_{%=o.i%}">
                        @foreach($object->translatedAttributes as $name)
                            @if(isset($fields[$name]) && View::exists('crud::fields.'.$fields[$name]['type']) && $name !== $module
                                && $name !== str_singular($module) && $name !== str_singular($module).'_id')
                                @php
                                    $fields[$name]['lang'] = $short;
                                    $fields[$name]['label'] = $object->getFieldLabel($name);
                                    $fields[$name]['name'] = $relation.'[{%=o.i%}]['.$name.']';
                                @endphp
                                @include('crud::fields.'.$fields[$name]['type'], $fields[$name])
                            @endif
                        @endforeach
                    </div>
                    <!-- /.tab-pane -->
                @endforeach
            </div>
            <!-- /.tab-content -->
        </div>
    @endif
    @foreach($fields as $name => $field)
        @if(isset($object->translatedAttributes) && in_array($name, $object->translatedAttributes))
            @continue
        @endif
        @if(View::exists('crud::fields.'.$field['type']) && $name !== $module
            && $name !== str_singular($module) && $name !== str_singular($module).'_id')
            @php
                $field['label'] = $object->getFieldLabel($name);
                $field['name'] = $relation.'[{%=o.i%}]['.$name.']';
            @endphp
            @include('crud::fields.'.$field['type'], $field)
        @endif
    @endforeach
    <a class="remove-relation" href="javascript:void(0)">Remove {%=o.btn%}</a>
</div>