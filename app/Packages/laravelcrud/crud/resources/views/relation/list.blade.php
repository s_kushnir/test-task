@if($count > 0)
    <div class="table-responsive">
        <form action="" method="get">
            <div class="form-group">
                <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    {!! request_to_inputs(array_except(request()->query(), 'search.'.$route)) !!}
                    <input id="search" name="search[{{ $route }}]" type="text" class="form-control" value="{{ request()->input('search.'.$route) }}" placeholder="Search">
                </div>
            </div>
        </form>
        <table class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    @foreach($columns as $column)
                        <th>
                            @php
                                $parameters = array_except(request()->query(), 'sorting.'.$route);
                                $parameters['sorting'][$route] = [
                                        'column' => $column,
                                        'type' => 'asc'
                                    ];
                                $sorting = request()->input('sorting.'.$route);
                                $class = 'sorting';
                                if(is_array($sorting) && $sorting['column'] == $column) {
                                    $class = 'sorting_'.$sorting['type'];
                                    $parameters['sorting'][$route]['type'] = $sorting['type'] == 'desc' ? 'asc' : 'desc';
                                }
                                $parameters = array_merge($parameters, request()->route()->parameters());
                            @endphp
                            <a href="{{ route(request()->route()->getName(), $parameters)}}" class="{{ $class }}">
                                {{ $object->getFieldLabel($column) }}
                            </a>
                        </th>
                    @endforeach
                    @if(!isset($noNeedOptions))
                        <th>Options</th>
                    @endif    
                </tr>
            </thead>
            <tbody>
                @foreach($list as $item)
                    <tr>
                        @foreach($columns as $column)                      
                            @if(isset($item->fields[$column]['column_mutator']))
                                <td>{!! $item->{$item->fields[$column]['column_mutator']}() !!}</td>
                            @else
                                <td>{!! $item->{$column} !!}</td>
                            @endif
                        @endforeach
                        @if(!isset($noNeedOptions))
                        <td class="text-center nowrap">
                            <form action="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.destroy", ['id' => $item->id]) }}" method="POST" class="form-inline">
                                {!! method_field('DELETE') !!}
                                {!! csrf_field() !!}

                                <a class="btn btn-info" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.show", ['id' => $item->id]) }}" title="Show">
                                    <i class="fa fa-eye"></i> Show
                                </a>

                                <a class="btn btn-success" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.edit", ['id' => $item->id]) }}" title="Edit">
                                    <i class="fa fa-edit"></i> Edit
                                </a>

                                <a class="delete-confirm btn btn-danger" data-title="Delete" data-text="Are you sure?" title="Delete">
                                    <i class="fa fa-trash"></i> Delete
                                </a>
                            </form>
                        </td>
                        @endif 
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    @foreach($columns as $column)
                        <th>{{ $object->getFieldLabel($column) }}</th>
                    @endforeach
                    @if(!isset($noNeedOptions))
                        <th>Options</th>
                    @endif 
                </tr>
            </tfoot>
        </table>
    </div>
    {!! $list->appends(array_except(request()->query(), $route))->links(); !!}
@else
    None
@endif