@extends('crud::layout')

@section('content_title')
    List
@endsection

@section('content_options')
    {!! $top ?? '' !!}
    @php
        $creatable = !isset($object->creatable) || (isset($object->creatable) && $object->creatable === true);
    @endphp
    @if($filters)
        <!-- general form elements -->
        <div class="box box-info">
            <div class="box-body with-border">
                <form action="" method="get">
                    {!! request_to_inputs(array_except(request()->query(), 'filters')) !!}
                    @foreach($filters as $name => $field)
                        <div class="col-md-3 col-sm-4">
                            @if(View::exists('crud::fields.'.$field['type']))
                                @php
                                    $filters_request = request('filters');
                                    $object->{"filters[$name]"} = $filters_request[$name] ?? '';
                                    $field['label'] = $object->getFieldLabel($name);
                                    $name = "filters[$name]";
                                @endphp
                                @include('crud:fields.'.$field['type'], $field)
                            @endif
                        </div>
                    @endforeach
                    @if(!empty(request()->all()))
                    <div class="form-group pull-right" style="margin-right: 15px;">
                        <label for=""><br></label><br>
                        <a class="btn btn-danger" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.index", array_except(request()->query(), 'filters')) }}">
                            <i class="fa fa-close" aria-hidden="true"></i> Cancel
                        </a>
                    </div>
                    @endif
                    <div class="form-group pull-right" style="margin-right: 15px;">
                        <label for=""><br></label><br>
                        <button class="btn btn-success" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Apply</button>
                    </div>
                </form>
            </div>
        </div>
    @endif
    @if($actions)
        <!-- general form elements -->
        <div class="box box-success">
            <div class="box-body with-border">
                @foreach($actions as $action)
                    <a href="{{ route($action['route_name'], request()->only(['filters', 'search'])) }}" class="btn btn-success">{{ $action['label'] }}</a>
                @endforeach
            </div>
        </div>
    @endif
    @if($creatable)
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-body with-border">
                @if($creatable)
                    <a class="btn btn-primary" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.create") }}" title="Create">
                        <i class="fa fa-plus"></i> Create
                    </a>
                @endif
            </div>
        </div>
    @endif  
    <!-- /.box -->
@endsection

@section('content_body')
    <div class="box-body table-responsive">
        @if(count($list) > 0)          
            <form action="" method="get">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                        {!! request_to_inputs(array_except(request()->query(), 'search')) !!}
                        <input id="search" name="search" type="text" class="form-control" value="{{ request('search') }}" placeholder="Search">
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        @foreach($columns as $column)
                            <th>
                                @php
                                    $parameters = array_except(request()->query(), 'sorting');
                                    $parameters['sorting'] = [
                                            'column' => $column,
                                            'type' => 'asc'
                                        ];

                                    $sorting = request('sorting');
                                    $class = 'sorting';
                                    if(is_array($sorting) && $sorting['column'] == $column) {
                                        $class = 'sorting_'.$sorting['type'];
                                        $parameters['sorting']['type'] = $sorting['type'] == 'desc' ? 'asc' : 'desc';
                                    }
                                @endphp
                                <a href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.index", $parameters)}}" class="{{ $class }}">
                                    {{ $object->getFieldLabel($column) }}
                                </a>
                            </th>
                        @endforeach
                        <td><strong>Options</strong></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $item)
                        <tr>
                            @foreach($columns as $column)
                                @if(isset($item->fields[$column]['column_mutator']))
                                    <td>{!! $object->getHtml($item->{$item->fields[$column]['column_mutator']}()) !!}</td>
                                @else
                                    <td>
                                        @if($item->fields[$column]['type'] == 'bool')
                                            @if($item->{$column} == 1)
                                                <span class="label label-success">True</span>
                                            @else
                                                <span class="label label-danger">False</span>
                                            @endif
                                        @else
                                            {!! $object->getHtml($item->{$column}) !!}
                                        @endif
                                    </td>
                                @endif
                            @endforeach
                            <td class="text-center nowrap">
                                @php
                                    $editable = !isset($item->editable) || (isset($item->editable) && $item->editable === true);
                                    $deletable = !isset($item->deletable) || (isset($item->deletable) && $item->deletable === true);
                                    $viewable = !isset($item->viewable) || (isset($item->viewable) && $item->viewable === true);
                                @endphp
                                <form action="{{ $deletable ? route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.destroy", ['id' => $item->id]) : '' }}" method="POST" class="form-inline">
                                    {!! method_field('DELETE') !!}
                                    {!! csrf_field() !!}

                                    @if($viewable)
                                        <a class="btn btn-info" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.show", ['id' => $item->id]) }}" title="Show">
                                            <i class="fa fa-eye"></i> Show
                                        </a>
                                    @endif

                                    @if($editable)
                                        <a class="btn btn-success" href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.edit", ['id' => $item->id]) }}" title="Edit">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    @endif

                                    @if($deletable)
                                        <a class="delete-confirm btn btn-danger" data-title="Delete" data-text="Are you sure?" title="Delete">
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    @endif

                                    @if($item->buttons)
                                        @foreach($item->buttons as $btn)
                                            @php
                                                $data = [];
                                                if(isset($btn['data'])) {
                                                    foreach ($btn['data'] as $k => $v) {
                                                        $data[] = "data-{$k}=\"{$v}\"";
                                                    }
                                                }
                                                $data = implode($data, ' ');
                                            @endphp
                                            <a class="{{ $btn['classes'] }}" href="{{ $btn['url'] }}" {!! $data !!}>
                                                {!! $btn['label'] !!}
                                            </a>
                                        @endforeach
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        @foreach($columns as $column)
                            <th>{{ $object->getFieldLabel($column) }}</th>
                        @endforeach
                        <th>Options</th>
                    </tr>
                </tfoot>
            </table>
            {!! $list->appends(array_except(request()->query(), $route))->links(); !!}
        @else
            <div class="callout callout-info">
                <h4>No data to display!</h4>
                @if($creatable)
                    <p>Please <a href="{{ route((config('crud.prefix') ? config('crud.prefix') . '.' : '')."$route.create") }}">create new item</a>.</p>
                @endif
            </div>
        @endif
    </div>
@endsection