@php
    $old = $object->getOld($name, $old ?? null, $lang ?? null);
@endphp
<div class="form-group clearfix @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label><br>
    {{-- Todo: change old data --}}
    <input type="file" class="form-control" data-aspect-ratio="{{ $aspect_ratio }}" value="">
    @php
        if($object->{$name} && (empty($old) || !isset($old['cropped']) || !isset($old['original'])) ) {
            $old = [];
            $pieces = explode('.', $object->{$name});
            $old['original'] = 'data:image/' . end($pieces) . ';base64,' . base64_encode(Storage::disk('public')->get($object->{$name}));
            $old['cropped'] = 'data:image/' . end($pieces) . ';base64,' . base64_encode(Storage::disk('public')->get(thumb($object->{$name})));
        } elseif(empty($object->{$name}) && empty($old)) {
            $old = [
                'original' => '',
                'cropped' => '',
            ];
        }
    @endphp
    <input type="hidden" name="{{  $object->getFieldName($name, $lang ?? null) }}[cropped]" class="cropped-image"
           value="{{ $old['cropped'] }}">
    <input type="hidden" name="{{  $object->getFieldName($name, $lang ?? null) }}[original]" class="original-image"
           value="{{ $old['original'] }}">
    <p class="help-block">{!! $help ?? '' !!}</p>
    <div class="pull-left img-preview" style="display: none">
        <a href="javascript:void(0)" class="btn btn-danger delete-img"><i class="fa fa-close"></i></a>
        <a href="javascript:void(0)" class="btn btn-default crop-img"><i class="fa fa-crop"></i></a>
    </div>
</div>
@section('js')
    <script>
        'use strict';

        var saveOriginal = true;

        $(document).ready(function () {
            $(document).on('change', 'input[type="file"]', function() {
                loadImage(this);
            });

            $(document).on('click', '.delete-img', function(e) {
                e.preventDefault();
                var formGroup = $(this).parents('.form-group');
                formGroup.find('.img-thumbnail, .img-cropped').remove();
                formGroup.find('input[type="file"]')[0].outerHTML = formGroup.find('input[type="file"]')[0].outerHTML;
                formGroup.find('.img-preview').hide();
            });

            $(document).on('click', '.crop-img', cropImage);
            loadOld();
        });

        function loadOld() {
            $('input[type="file"]').each(function () {
                var original = $(this).siblings('input[name*="original"]').val();
                var cropped = $(this).siblings('input[name*="cropped"]').val();
                if(original) {
                    $(this).siblings('.img-preview').show().append('<img src="' + original + '" class="pull-left img-thumbnail" ' +
                        'data-toggle="tooltip" data-placement="top" title="Original">')
                }
                if(cropped) {
                    $(this).siblings('.img-preview').after('<div class="pull-left img-cropped">\n' +
                        '   <img class="img-thumbnail" src="' + cropped + '" data-toggle="tooltip" data-placement="top" title="Cropped">\n' +
                        '</div>')
                }

            })
        }

        function loadImage(input) {
            if (input.files && input.files[0]) {
                var  formGroup = $(input).parents('.form-group')
                formGroup.find('.img-thumbnail').remove();
                var reader = new FileReader();
                reader.onload = function(e) {
                    formGroup.find('.img-thumbnail, .img-cropped').remove();
                    var image = new Image();
                    image.onload = function () {
                        formGroup.find('.original-image').val($(this).attr('src'));
                    };
                    image.src = e.target.result;
                    image.className = 'pull-left ' +
                        'img-thumbnail';
                    image.setAttribute('data-toggle', 'tooltip');
                    image.setAttribute('data-placement', 'top');
                    image.setAttribute('title', 'Original');
                    var wrap = $(input).siblings('.img-preview');
                    wrap[0].appendChild(image);
                    wrap.show();

                    input.outerHTML = input.outerHTML;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function cropImage(e) {
            e.preventDefault();
            var btn = $(this);
            var image = btn.siblings('.img-thumbnail').clone();
            var params = {
                height: 0,
                scaleX: 1,
                scaleY: 1,
                width: 0,
                x: 0,
                y: 0
            };
            // Build image modal
            var modal = $('<div class="modal fade">\n' +
                '  <div class="modal-dialog">\n' +
                '    <div class="modal-content">\n' +
                '      <div class="modal-header">\n' +
                '        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>\n' +
                '        <h4 class="modal-title">Crop image</h4>\n' +
                '      </div>\n' +
                '      <div class="modal-body">\n' +
                '        <center></center>' +
                '      </div>\n' +
                '      <div class="modal-footer">\n' +
                '        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>\n' +
                '        <button type="button" class="btn btn-primary crop-apply">Apply</button>\n' +
                '      </div>\n' +
                '    </div><!-- /.modal-content -->\n' +
                '  </div><!-- /.modal-dialog -->\n' +
                '</div><!-- /.modal -->')
            modal.find('.modal-body > center')
                .append(image);
            image.wrap('<div style="display: inline-block"></div>');
            modal.modal({backdrop: 'static', keyboard: false})
                .on('shown.bs.modal', function() {
                    // Init cropping
                    new Cropper(image[0], {
                        aspectRatio: btn.parents('.form-group').find('input[type="file"]').data('aspect-ratio'), // Todo: parameter for aspect ratio
                        zoomable: false,
                        rotatable: false,
                        crop: function(e) {
                            params = e.detail;
                        }
                    });
                })
                .on('hidden.bs.modal', function() {
                    $(this).siblings('.modal-backdrop.in').remove();
                    $(this).remove();
                });
            // Apply cropping
            modal.find('.crop-apply').click(function(e) {
                e.preventDefault();
                // Create canvas for load the image
                var canvas = document.createElement("canvas");

                canvas.width = image[0].width;
                canvas.height = image[0].height;

                var context = canvas.getContext("2d");
                context.drawImage(image[0], 0, 0, image[0].width, image[0].height);
                // get the current ImageData for the canvas
                var data = context.getImageData(params.x, params.y, params.width, params.height);

                // create canvas for the cropped image
                var tempCanvas = document.createElement("canvas"),
                    tCtx = tempCanvas.getContext("2d");

                tempCanvas.width = params.width;
                tempCanvas.height = params.height;

                tCtx.putImageData(data, 0, 0);

                // write on the screen
                var cropped = tempCanvas.toDataURL();
                if(saveOriginal) {
                    btn.parent().siblings('.img-cropped').remove();
                    btn.parent().after('<div class="pull-left img-cropped"><img class="img-thumbnail" src="' + cropped + '"' +
                        ' data-toggle="tooltip" data-placement="top" title="Cropped"></div>');
                } else {
                    btn.siblings('.img-thumbnail').attr('src', cropped).attr('title', 'Cropped');
                }
                $('.cropped-image').val(cropped);

                modal.modal('hide');
            })
        }
    </script>
@stop