@php
    $id_editor = md5(time().str_random(5));
@endphp
<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <textarea id="{{ $id_editor }}" placeholder="Place some text here" name="{{  $object->getFieldName($name, $lang ?? null) }}"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $object->getOld($name, $old ?? null, $lang ?? null) }}</textarea>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>
@push('js')
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('{{ $id_editor }}');
        });
    </script>
@endpush
