<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <input type="url" name="{{  $object->getFieldName($name, $lang ?? null) }}" class="form-control" value="{{ $object->getOld($name, $old ?? null, $lang ?? null) }}">
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>