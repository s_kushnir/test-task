<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <select name="{{  $object->getFieldName($name, $lang ?? null) }}" class="form-control" @if(isset($disabled) && $disabled) disabled @endif>
        @php
            $options = $object->{$selector}()
        @endphp
        @foreach($options as $option)
            <option value="{{ $option['value'] }}" @if(old($old ?? $name, $object->{$name}) == $option['value']) selected @endif>{{ $option['label'] }}</option>
        @endforeach
    </select>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>