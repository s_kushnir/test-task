@php
    $id_editor = time().str_random(5);
@endphp
<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <textarea class="hidden" placeholder="Place some text here" name="{{  $object->getFieldName($name, $lang ?? null) }}"
                          >{!! old($old ?? $name, $object->{$name}) !!}</textarea>
    <div>
        <a href="#" class="btn btn-primary add-columns" data-count="1" role="button">1 column</a>
        <a href="#" class="btn btn-primary add-columns" data-count="2" role="button">2 columns</a>
        <a href="#" class="btn btn-primary add-columns" data-count="3" role="button">3 columns</a>
    </div>
    <div class="inline-editor">

    </div>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>
@section('js')
    @parent
    <script>
        // Turn off automatic editor creation first.
        CKEDITOR.disableAutoInline = true;
        $(document).ready(function(){
           $('.add-columns').click(function(e){
               e.preventDefault();
               var section = '';
               switch($(this).data('count')) {
                   case 1:
                       section = '<div class="row"><div class="col-sm-12 inline-column" contenteditable="true">Some text</div></div>';
                       break;
                   case 2:
                       section = '<div class="row"><div class="col-sm-6 inline-column" contenteditable="true">Some text</div>' +
                           '<div class="col-sm-6 inline-column" contenteditable="true">Some text</div></div>';
                       break;
                   case 3:
                       section = '<div class="row"><div class="col-sm-4 inline-column" contenteditable="true">Some text</div>' +
                           '<div class="col-sm-4 inline-column" contenteditable="true">Some text</div>' +
                           '<div class="col-sm-4 inline-column" contenteditable="true">Some text</div></div>';
                       break;
               }
               section = $(section);
               $(this).parents('.form-group').find('.inline-editor').append(section);
               console.log(section);
               section.find('.inline-column').each(function(){
                   console.log(1, CKEDITOR.inline(this));
               })
           });
           $(document).on('keydown', '.inline-column', function(){
               $(this).parents('.form-group').find('textarea.hidden')
                   .val($(this).parents('.inline-editor').html());
           })
        });
    </script>
@stop
