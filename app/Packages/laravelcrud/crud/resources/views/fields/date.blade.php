<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input type="text" name="{{  $object->getFieldName($name, $lang ?? null) }}" class="form-control datetimepicker" data-format="{{ $format }}" value="{{ $object->getOld($name, $old ?? null, $lang ?? null) }}">
    </div>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>
