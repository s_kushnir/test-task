<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
        <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
        <label class="switch">
            <input type="checkbox" name="{{  $object->getFieldName($name, $lang ?? null) }}" value="1" @if( old($old ?? $name, $object->{$name}) == 1 ) checked @endif >
            <span class="slider round"></span>
        </label>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>