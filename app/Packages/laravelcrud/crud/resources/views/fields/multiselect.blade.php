<div class="form-group @if($errors->has($old ?? (isset($lang) ? $lang . '.' : '').$name))) has-error @endif">
    <label for="">{{ $label ?? $object->getFieldLabel($name) }}</label>
    <select name="{{ $name }}[]" class="form-control" multiple>
        @php
            $options = $object->{$selector}();
            $values = old($old ?? $name, isset($form_mutator) ? $object->{$form_mutator}() : $object->{$name}) ?? [];
        @endphp
        @foreach($options as $option)
            <option value="{{ $option['value'] }}" @if(in_array($option['value'], $values)) selected @endif>{{ $option['label'] }}</option>
        @endforeach
    </select>
    <p class="help-block">{!! $help ?? '' !!}</p>
</div>