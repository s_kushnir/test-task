@extends('adminlte::page')

@section('content_header')
    <h1>{{ $title }}<small>{{ $subtitle ?? '' }}</small></h1>
@endsection

@section('title_prefix')
    {{ $title }} |
@endsection

@section('content')

    <div id="overlay">
        <div id="progstat">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
            </div>
        </div>
        <div id="progress"></div>
    </div>

    <div class="alert alert-success absolute-styles" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <span></span>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {!! Session::get('message') !!}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {!! Session::get('error') !!}
        </div>
    @endif
    @if(Session::has('warning'))
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {!! Session::get('warning') !!}
        </div>
    @endif
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">

            @yield('content_options')

            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">@yield('content_title')</h3>
                </div>

                @yield('content_body')
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/cropperjs/dist/cropper.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <link rel="stylesheet" href="{{asset('css/preloader.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@endpush

@push('js')
    <script src="{{asset('js/preloader.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="{{asset('vendor/adminlte/plugins/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/select2/dist/js/select2.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/simple-ajax-uploader/SimpleAjaxUploader.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/cropperjs/dist/cropper.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/confirm/jquery.confirm.min.js')}}"></script>
    <script src="{{asset('vendor/adminlte/plugins/blueimp-tmpl/js/tmpl.min.js')}}"></script>

    <script src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/ckeditor/plugins/imageuploader/plugin.js') }}"></script>

    <script>

        if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
            CKEDITOR.tools.enableHtml5Elements( document );

        // The trick to keep the editor in the sample quite small
        // unless user specified own height.
        CKEDITOR.config.height = 150;
        CKEDITOR.config.width = 'auto';
        CKEDITOR.config.extraPlugins = 'imageuploader';


        $(document).ready(function(){
            
            $('.datetimepicker').each(function(){
                $(this).datetimepicker({
                    format: $(this).data('format') || 'YYYY-MM-DD HH:mm a Z'
                });
            });

            $('select.form-control').select2();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                }
            });

            deleteInit();
            setHistory();

            $(document).on('click', '.back', function(e) {
                if(localStorage.history) {
                    var history = JSON.parse(localStorage.history);
                    e.preventDefault();
                    for(var i = 0; i < history.length; i++) {
                        if(history[i].current && history[i - 1]) {
                            history[i - 1].current = true;
                            history.length = i + 1;
                            localStorage.setItem('history', JSON.stringify(history));
                            document.location.href = history[i - 1].url;
                            break;
                        }
                    }
                }
            });

            var formSubmitting = false;

            $(document).on('submit', 'form', function() {
                formSubmitting = true;
            });

        });

        /**
         * The function for deletion confirm init
         */
        function deleteInit() {
            $('.delete-confirm').each(function () {
                var element = $(this);
                element.confirm({
                    theme: 'bootstrap',
                    title: element.data('title'),
                    text: element.data('text'),
                    confirm: function (button) {
                        element.parents('form').submit();
                    },
                    confirmButton: "Yes I am",
                    cancelButton: "No",
                    post: true,
                    confirmButtonClass: "btn-success",
                    cancelButtonClass: "btn-danger",
                });
            });
        }

        /**
         * The function for history saving. It save to 10 steps.
         */
        function setHistory() {
            var history = [];
            var currentUrl = document.location.href.split('&')[0];
            if(localStorage.history && document.location.href.split('/').length > 5) {
                history = JSON.parse(localStorage.history);

                if(currentUrl !== document.referrer && history[history.length -1].url !== currentUrl && history[history.length -1].current === true) {

                    history[history.length -1].current = false;

                    history.push({
                        url: currentUrl,
                        current: true
                    });
                }

                if(history.length > 10) {
                    history = history.shift();
                }
            } else {
                history = [{
                    url: currentUrl,
                    current: true
                }];
            }
            localStorage.setItem('history', JSON.stringify(history));
        }

    </script>
@endpush