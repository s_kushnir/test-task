<?php

namespace Laravelcrud\Crud;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\ServiceProvider as AdminLte;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Dispatcher $events
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            foreach (config('crud.menu') as $item) {
                if(!isset($item['may']) || auth(config('crud.guard'))->user()->can($item['may'])) {
                    $item['url'] = route($item['route']);
                    $event->menu->add($item);
                }
            }
        });

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'crud');

        $this->publishes([
            __DIR__.'/../views' => base_path('resources/views/laravelcrud/crud'),
        ]);

        config([
            'adminlte.dashboard_url' => '/',
            'adminlte.menu' => [],
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/Helpers.php';
        $this->app->register(AdminLte::class);
    }
}
