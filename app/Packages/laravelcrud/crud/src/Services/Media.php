<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 1/29/18
 * Time: 11:18 AM
 */
namespace Laravelcrud\Crud\Services;

use Illuminate\Support\Facades\Storage;

class Media {

    public static function save($base64, $path, $name) {
        $pattern = '/data\:image\/([a-z]{3,4})\;base64\,/i';
        preg_match($pattern, $base64, $match);
        $decoded_file = base64_decode(preg_replace($pattern, '', $base64));
        if(empty($match)) {
            $extension = $match[1];
        } else {
            $extension = 'png';
        }
        $storage_path = "$path/$name.$extension";
        if(Storage::disk('public')->put($storage_path, $decoded_file, 'public')) {
            return $storage_path;
        }
    }

    public static function analyze($request, $fields, $path) {
        $files_field = array_filter($fields, function($field) {
            return $field['type'] == 'file';
        });

        foreach($files_field as $name => $data) {
            $file_name = md5(time());
            if(isset($request[$name]['original'])) {
                $file_original = self::save($request[$name]['original'], $path, $file_name);
            }
            if(isset($request[$name]['cropped'])) {
                $file_name .= '_thumb';
                $file_cropped = self::save($request[$name]['cropped'], $path, $file_name);
            }
            $request[$name] = isset($file_original) ? $file_original : (
                isset($file_cropped) ? $file_cropped : '');
        }
        return $request;
    }
}