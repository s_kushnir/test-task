<?php

namespace Laravelcrud\Crud;

use Exception;
use App;
use DB;
use Carbon\Carbon;
use Laravelcrud\Crud\Services\Media;
use Storage;
use Route;

trait Crud
{
    protected $object;

    /**
     * This factory method for data generation
     *
     * @return array
     */
    public function data()
    {
        $fields = $this->fields;
        $back_url = Route::has(config('crud.prefix').'.'. ( $this->routeSelector ?? $this->getTable() ) .'.index') ?
                route(config('crud.prefix').'.'. ( $this->routeSelector ?? $this->getTable() ) .'.index') : '';
        $columns = $this->columns;
        $route = $this->routeSelector ?? $this->getTable();
        $object = $this;
        $relation = $this->set_relation ?? null;
        $class = self::class;

        return compact('fields', 'back_url', 'columns', 'route', 'object', 'relation', 'class');
    }

    /**
     * Get list of records
     *
     * @param mixed $query
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function getList($query = null)
    {
        $request = request();
        $sorting = $request->input('sorting');
        if(is_array($sorting)) {
            if(\Schema::hasColumn($this->getTable(), $sorting['column']) &&
                !isset($this->fields[$sorting['column']]['sorting_query'])) {

                $list = self::orderBy($this->getTable() . '.' . $sorting['column'], $sorting['type']);
            } elseif(isset($this->translatedAttributes) && in_array($sorting['column'], $this->translatedAttributes)) {
                $table = $this->getTable();
                $list = $this->join($this->getTranslationsTable() . ' as t', function ($join) use ($sorting, $table) {
                    $join->on($table . '.id', '=', 't.' . str_singular($table) . '_id')
                        ->where('t.locale', '=', app()->getLocale());
                    })
                    ->orderBy('t.' . $sorting['column'], $sorting['type']);
            } else {
                $list = $this->{$this->fields[$sorting['column']]['sorting_query']}($sorting['type']);
            }
        } else {
            $list = self::orderBy($this->getTable() . '.updated_at', 'desc');
        }
        $list = $this->search($list);
        $list = $this->filtered($list);
        if($query) {
            $list = $list->where($query);
        }
        $list = $list->paginate(10);
        return view('crud::list', $this->data())
            ->with('list', $list)
            ->with('filters', $this->filters ?? null)
            ->with('actions', $this->actions ?? null);
    }

    /**
     * Get creation form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreateForm()
    {
        return view('crud::create', $this->data());
    }

    /**
     * Get preview data
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPreview($id)
    {
        $object = self::find($id);
        return view('crud::show', $object->data());
    }

    /**
     * Get edition form
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditForm($id)
    {
        $object = self::find($id);
        return view('crud::edit', $object->data());
    }

    /**
     * This method stores data to DB
     *
     * @param CrudRequest $request
     * @param array $relations
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createObject($request, Array $relations = [])
    {
        DB::beginTransaction();
        try {
            $except = ['_token'];
            if(isset($this->set_relation)) {
                $except[] = $this->set_relation;
                $relation_data = $request->input($this->set_relation);
            }
            $fields = $request->except($except);
            // Todo: refactor it
            $fields = Media::analyze($fields, $this->fields, $this->routeSelector ?? $this->getTable());
            $object = self::create($fields);
            // Sync relations
            if(! empty($relations)) {
                foreach ($relations as $relation => $data) {
                    // Todo: fix sync error
                    if(isset($this->exists_relation_updated_at)) {
                        $array = $data;
                        $data = [];
                        foreach ($array as $id => $value) {
                            if(is_array($value)) {
                                $data[$id] = $value;
                                $data[$id]['updated_at'] = Carbon::now();
                            } else {
                                $data[$value] = ['updated_at' => Carbon::now()];
                            }
                        }
                    }

                    $object->{$relation}()->sync($data);
                }
            }
            if(isset($this->set_relation) && $relation_data) {
                $relation_fields = [];
                foreach ($relation_data as $item) {
                    $relation_object = $object->{$this->set_relation}()->getRelated();
                    $relation_fields[] = Media::analyze($item, $relation_object->fields, $relation_object->routeSelector ?? $relation_object->getTable());
                }
                $object->{$this->set_relation}()->createMany($relation_fields);
            }
            DB::commit();
            $data = $this->data();
            $prefix = config('crud.prefix') ? config('crud.prefix') . '.' : '';
            $url = route("{$prefix}{$data['route']}.show", ['id' => $object->id]);
            $this->object = $object;
            return redirect()->route(config('crud.prefix').'.'. ( $this->routeSelector ?? $this->getTable() ) .'.index')
                ->with('message', "This <a href=\"$url\">record</a> has been successfully created!");
        }
        catch (Exception $e) {
            DB::rollBack();
            $error = $this->errorMessage($e);
            return redirect()->back()
                ->with('error', $error);
        }
    }

    /**
     * This method updates data to DB
     *
     * @param int $id
     * @param CrudRequest $request
     * @param array $relations
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateObject($id, $request, Array $relations = [])
    {
        DB::beginTransaction();
        try {
            $object = self::find($id);
            $except = ['_token', '_method'];
            if(isset($this->set_relation)) {
                $except[] = $this->set_relation;
                $relation_data = array_values($request->input($this->set_relation, []));
            }
            $fields = $request->except($except);
            $fields = Media::analyze($fields, $this->fields, $this->routeSelector ?? $this->getTable());
            // Modify date before insert
            foreach ($fields as $name => $value) {
                if(array_key_exists($name, $object->toArray())) {
                    $object->{$name} = $value;
                }
            }
            if($object->isDirty()) {
                $object->save();
            }
            if(isset($this->translatedAttributes)) {
                foreach (config('crud.languages') as $short => $language) {
                    foreach ($fields[$short] as $name => $value) {
                        $object->getTranslationOrNew($short)->{$name} = $value;
                    }
                    if ($object->getTranslationOrNew($short)->isDirty()) {
                        $object->getTranslationOrNew($short)->save();
                    }
                }
            }
            // Sync relations
            if(! empty($relations)) {
                foreach ($relations as $relation => $data) {
                    // Todo: fix sync error
                    if(isset($object->exists_relation_updated_at)) {
                        $array = $data;
                        $data = [];
                        foreach ($array as $id => $value) {
                            if(is_array($value)) {
                                $data[$id] = $value;
                                $data[$id]['updated_at'] = Carbon::now();
                            } else {
                                $data[$value] = ['updated_at' => Carbon::now()];
                            }
                        }
                    }

                    $object->{$relation}()->sync($data);
                }
            }
            if(isset($this->set_relation)) {
                foreach ($object->{$this->set_relation} as $item) {
                    $index = array_search($item->id, array_column($relation_data, 'id'));
                    if($index !== false && isset($relation_data[$index])) {
                        foreach ($relation_data[$index] as $key => $value) {
                            $item->{$key} = $value;
                        }
                        if($item->isDirty()) {
                            $item->save();
                        }
                        unset($relation_data[$index]);
                    } else {
                        $item->delete();
                    }
                }
                if(!empty($relation_data)) {
                    $object->{$this->set_relation}()->createMany($relation_data);
                }
            }
            DB::commit();
            $data = $this->data();
            $prefix = config('crud.prefix') ? config('crud.prefix') . '.' : '';
            $url = route("{$prefix}{$data['route']}.show", ['id' => $object->id]);
            return redirect()->route(config('crud.prefix').'.'. ( $object->routeSelector ?? $object->getTable() ) .'.index')
                ->with('message', "This <a href=\"$url\">record</a> has been successfully updated!");
        }
        catch (Exception $e) {
            DB::rollBack();
            $error = $this->errorMessage($e);
            return redirect()->back()
                ->with('error', $error);
        }
    }

    /**
     * This method deletes data to DB
     *
     * @param int $id
     * @param array $cascadeRelations
     * @param array $noActionsRelations
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteObject($id, Array $cascadeRelations = [], Array $noActionsRelations = [])
    {
        DB::beginTransaction();
        try {
            $object = self::find($id);
            foreach ($cascadeRelations as $name) {
                $object->{$name}()->delete();
            }
            $object->delete();
            DB::commit();
            $prevision = app('router')->getRoutes()->match(app('request')->create(redirect()->back()->getTargetUrl()))->getName();
            $page = explode('.', $prevision);
            $page = array_pop($page);
            $redirect = redirect()->back();
            if($page == 'show') {
                $data = $this->data();
                $redirect = redirect()->route("crud.$data[route].index");
            }
            return $redirect->with('message', "This record has been successfully deleted!");
        } catch (Exception $e) {
            DB::rollBack();
            
            // validation for constrain errors
            if($e instanceof \Illuminate\Database\QueryException){
                if(count($noActionsRelations) > 0){                  
                    foreach ($noActionsRelations as $key => $value) {  
                        $stringRelations[] = ($object->{$key}() instanceof \Illuminate\Database\Eloquent\Relations\BelongsTo ? 1 : count($object->{$key})).' '.$value.', ';
                    }   
                    $error = "In order to delete this item, please remove the following relations: ".substr(implode('', $stringRelations), 0, -2);
                }else{
                    $error = $this->errorMessage($e);
                }
            }else {
                $error = $this->errorMessage($e);
            }
            return redirect()->back()
                ->with('error', $error);
        }
    }

    /**
     * Wrapper for errors message
     *
     * @param Exception $e
     * @return string
     */
    public function errorMessage(Exception $e) {
        if (App::environment() !== 'production') {
            return "Message: {$e->getMessage()}, File: {$e->getFile()}, Line: {$e->getLine()}";
        } else {
            return 'Sorry! Something went wrong!';
        }
    }

    /**
     * Get relation data on table
     *
     * @param $relatedCollection
     * @return mixed
     * @throws \Throwable
     */
    public function getRelationTable($relatedCollection)
    {
        $data = $this->data();
        $request = request();
        $sorting = $request->input('sorting.'.$data['route']);
        if(is_array($sorting)) {
            if(\Schema::hasColumn($this->getTable(), $sorting['column']) &&
                !isset($this->fields[$sorting['column']]['sorting_query'])) {
                $list = $relatedCollection->orderBy($this->getTable() . '.' . $sorting['column'], $sorting['type']);
            } else {
                $list = $this->{$this->fields[$sorting['column']]['sorting_query']}($sorting['type']);
                $clone = clone $relatedCollection;
                $list->whereIn($this->getTable().'.id', array_column($clone->get()->toArray(), 'id'));
            }
        } else {
            $list = $relatedCollection->orderBy($this->getTable() . '.updated_at', 'desc');
        }
        $needle = $request->input('search.'.$data['route']);
        if($needle) {
            $list = $this->search($list, $needle);
        }
        $list = $list->paginate(5, ['*'], $data['route']);
        return view('crud::relation.list', $data)
            ->with('list', $list)
            ->with('count', count($list))->render();
    }

    /**
     * Get relation create form
     *
     * @param string $name
     * @param string $class
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRelationCreateForm($name, $class)
    {
        $object = new $class;
        return view('crud::relation.create', $this->data())
            ->with('relation', $name)
            ->with('module', $object->getTable());
    }

    /**
     * Get relation edit form
     *
     * @param string $name
     * @param string $class
     * @param $item
     * @param int $index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRelationEditForm($name, $class, $item, $index)
    {
        $object = new $class;
        return view('crud::relation.edit', $this->data())
            ->with('relation', $name)
            ->with('module', $object->getTable())
            ->with('item', $item)
            ->with('index', $index);
    }

    /**
     * Mutator for created_at
     *
     * @return string
     */
    public function getCreatedAt() {
        return Carbon::parse($this->created_at)->format('D, M d, Y \a\t h:i:s a');
    }

    /**
     * Mutator for updated_at
     *
     * @return string
     */
    public function getUpdatedAt() {
        return Carbon::parse($this->updated_at)->format('D, M d, Y \a\t h:i:s a');
    }

    /**
     * Get label
     *
     * @param string $name
     * @return string
     */
    public function getFieldLabel($name) {
        return isset($this->fields[$name]['label']) ? $this->fields[$name]['label'] : ucfirst(str_replace('_', ' ', $name));
    }

    /**
     * Get field name
     *
     * @param string $name
     * @param bool|string $lang
     * @return string
     */
    public function getFieldName($name, $lang = false) {
        preg_match('/\[([a-z0-9]+)\]$/', $name, $matches);
        if(empty($matches)) {
            return $lang ? "{$lang}[{$name}]" : $name;
        } else {
            return $lang ? str_replace($matches[0], "[{$lang}]{$matches[0]}", $name) : $name;
        }
    }

    /**
     * Get old value
     *
     * @param string $name
     * @param null|string $old
     * @param bool|string $lang
     * @return string
     */
    public function getOld($name, $old = null, $lang = false) {
        preg_match('/^([a-z]+)\[([\d]+)\]\[([a-z]+)\]$/i', $name, $matches);
        if(!empty($matches) && $lang) {
            $old = old($matches[1], false);
            if($old && isset($old[$matches[2]]) && isset($old[$matches[2]][$lang][$matches[3]]) ) {
                return $old[$matches[2]][$lang][$matches[3]];
            }
            return $this->getTranslationOrNew($lang)->{$matches[3]};
        }
        if($lang) {
            $old_value = old($lang);
            $old_value = $old_value[$name] ?? $this->getTranslationOrNew($lang)->{$name};
        } else {
            $old_value = old($old ?? $name, $this->{$name});
        }
        return $old_value;
    }

    /**
     * The function for search
     *
     * @param object $query
     * @param object|null $needle
     * @return mixed
     */
    public function search($query, $needle = null) {
        $needle = $needle ?? request()->input('search');
        if($needle && isset($this->searchable)) {
            $searchable = $this->searchable;
            $query = $query->where(function($query) use ($needle, $searchable) {
                foreach ($searchable as $column) {
                    if(is_array($column)) {
                        $query->orWhereHas($column['relation'], function($query) use ($needle, $column) {
                            $query->where($column['column'], 'like', '%'.$needle.'%');
                        });
                    } elseif(isset($this->translatedAttributes) && in_array($column, $this->translatedAttributes)) {
                        $query->orWhereTranslationLike($column, '%'.$needle.'%');
                    } else {
                        $query->orWhere($this->getTable() . '.' . $column, 'like', '%'.$needle.'%');
                    }
                }
            });
        }
        return $query;
    }

    /**
     * The function for data filtering
     *
     * @param object $list
     * @return mixed
     */
    public function filtered($list)
    {
        $filters = request('filters', []);
        if(isset($this->filters) && !empty($this->filters)) {
            foreach ($this->filters as $column => $option) {
                if(isset($option['query']) && isset($filters[$column]) && $filters[$column]) {
                    $list = $this->{$option['query']}($list, $filters[$column]);
                } elseif(isset($filters[$column]) && isset($filters[$column])) {
                    $list = $list->where($column, $option['action'] ?? '=', $filters[$column]);
                }
            }
        }
        return $list;
    }

    public function getHtml($html) {
        $needle = request('search');
        if($needle && $html) {
            $html = str_replace('&', '=and=', $html);
            $needle = str_replace('&', '=and=', $needle);
            $d = new \DOMDocument();
            $d->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
            $d->formatOutput=true;
            $d->encoding='UTF-8';

            $x = new \DOMXPath($d);

            foreach ($x->query('//text()') as $node) {
                $node->nodeValue = preg_replace('/(' . $needle . ')/i', '<span class="mark">${1}</span>', $node->nodeValue);
            }
            return str_replace(['&lt;', '&gt;', '=and='], ['<', '>', '&'], $d->saveHTML());
        } else {
            return $html;
        }
    }

    /**
     * Get object after saving record
     * 
     * @return mixed
     */
    public function getObject() {
        return $this->object;
    }

    /**
     * Check on exists permissions for menu item
     *
     * @param string $type
     * @return bool
     */
    private function existsPermissions($type) {
        $route_name = config('crud.prefix') . '.' . ($this->routeSelector ?? $this->getTable()) . '.' . $type;
        $menu = config('crud.menu');
        $array_key = array_search($route_name, array_column($menu, 'route'));
        return $array_key !== false && isset($menu[$array_key]['may']);
    }

    /**
     * Check permission for editing
     *
     * @return bool
     */
    public function getEditableAttribute() {
        return !($this->existsPermissions('edit') &&
            auth(config('crud.guard'))->user()->can('edit ' . ($this->routeSelector ?? $this->getTable())));
    }

    /**
     * Check permission for deleting
     *
     * @return bool
     */
    public function getDeletableAttribute() {
        return !($this->existsPermissions('delete') &&
            auth(config('crud.guard'))->user()->can('delete ' . ($this->routeSelector ?? $this->getTable())));
    }

    /**
     * Check permission for creation
     *
     * @return bool
     */
    public function getCreatableAttribute() {
        return !($this->existsPermissions('create') &&
            auth(config('crud.guard'))->user()->can('create ' . ($this->routeSelector ?? $this->getTable())));
    }
}