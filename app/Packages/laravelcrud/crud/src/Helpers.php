<?php

if (!function_exists('request_to_inputs')) {
    /*
    |--------------------------------------------------------------------------
    | get inputs form request
    |--------------------------------------------------------------------------
    */
    /**
     * Recursive build map for each element
     *
     * @param array $array
     * @param mixed $value
     * @param string $key
     * @return array
     */
    function recursiveBuildArrayMap(array $array, $value, $key) {
        if(is_array($value)) {
            foreach ($value as $k => $v) {
                $array = array_merge($array, recursiveBuildArrayMap($array, $v, $key.'.'.$k));
            }
        } else {
            return array_merge($array, [$key => $value]);
        }
        return $array;
    }

    /**
     * Build inputs list
     *
     * @param array $array
     * @return string
     */
    function request_to_inputs(array $array) {
        $new_array = [];
        foreach ($array as $key => $value) {
            $new_array = array_merge($new_array, recursiveBuildArrayMap($new_array, $value, $key));
        }
        $inputs = '';
        foreach ($new_array as $name => $value) {
            $one = preg_match('/\./', $name);
            $name = str_replace_first('.', '[', $name);
            $inputs .= '<input type="hidden" name="'.str_replace('.', '][', $name).
                ($one == 1 ? ']' : '').'" value="'.$value.'">';
        }
        return $inputs;
    }      
}
