<?php

namespace Laravelcrud\Crud\Http\Controllers;

use App\Http\Controllers\Controller;

abstract class CrudController extends Controller
{
    /**
     * Save model instance
     *
     * @var string
     */
    protected $model;

    /**
     * CrudController constructor.
     */
    public function __construct() {
        view()->share('title', config('app.name'));
    }

    /**
     * Display a listing of the resource.
     * GET
     *
     * @return Response
     */
    public function index()
    {
        return $this->model->getList();
    }

    /**
     * Show the form for creating a new resource.
     * GET
     *
     * @return Response
     */
    public function create()
    {
        return $this->model->getCreateForm();
    }

    /**
     * Display the specified resource.
     * GET
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->model->getPreview($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->model->getEditForm($id);
    }

    /**
     * The record deletion
     * DELETE
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->model->deleteObject($id);
    }
}
