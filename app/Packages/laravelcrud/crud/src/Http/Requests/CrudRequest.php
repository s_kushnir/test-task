<?php

namespace Laravelcrud\Crud\Http\Rquests;

use Illuminate\Foundation\Http\FormRequest;

abstract class CrudRequest extends FormRequest
{
    protected $all_attributes = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = $this->all_attributes;
        if(isset($this->model)) {
            $model = new $this->model;
            foreach ($model->fields as $name => $field) {
                $attributes[$name] = $model->getFieldLabel($name);
            }
        }
        return $attributes;
    }

    /**
     * Get custom attributes for language fields.
     *
     * @param array $rules
     * @return array
     */
    protected function getLanguageRules(Array $rules) {
        $new_rules = [];
        if(isset($this->model)) {
            $model = new $this->model;
            foreach ($rules as $field => $rule) {
                foreach (config('crud.languages') as $short => $lang) {
                    $key = "{$short}.{$field}";
                    $new_rules[$key] = $rule;
                    $this->all_attributes[$key] = $model->getFieldLabel($field) . ' for ' . $lang;
                }
            }
        }
        return $new_rules;
    }
}
