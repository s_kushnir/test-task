<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\NewRequest;
use App\Models\News;
use Laravelcrud\Crud\Http\Controllers\CrudController;

class NewController extends CrudController
{
    /**
     * NewController constructor.
     * @param News $model
     */
    public function __construct(News $model)
    {
        parent::__construct();
        view()->share('title', 'News');
        $this->model = $model;
    }


    /**
     * Store a newly created resource in storage.
     * POST /news
     *
     * @param NewRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewRequest $request)
    {
        $categories = $this->correction($request);
        return $this->model->createObject($request, compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /news/{id}
     *
     * @param NewRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewRequest $request, $id)
    {
        $categories = $this->correction($request);
        return $this->model->updateObject($id, $request, compact('categories'));
    }

    /**
     * The method for request correction
     *
     * @param $request
     * @return mixed
     */
    private function correction($request)
    {
        $categories = $request->input('list_of_categories', []);
        $fields = $request->except(['list_of_categories']);
        $request->replace($fields);
        return $categories;
    }
}
