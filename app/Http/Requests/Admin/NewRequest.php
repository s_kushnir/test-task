<?php

namespace App\Http\Requests\Admin;

use App\Models\News;
use Laravelcrud\Crud\Http\Rquests\CrudRequest;

class NewRequest extends CrudRequest
{
    /**
     * Model for name attributes building
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:2|max:255',
            'description' => 'required|string|min:2|max:1000',
            'text' => 'required|string|min:2',
            'image.original' => 'required',
            'image.cropped' => 'required',
        ];
    }
}
