<?php
/**
 * Created at 12/4/17 4:36 PM by Serhiy K
 * Copyright (c) 2017, Unified AV Ltd. All rights reserved.
 */

namespace App\Models;

use App\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravelcrud\Crud\Crud;

class User extends Authenticatable
{
    use Notifiable, Crud;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    public $fields = [
        'first_name' => [
            'type' => 'string',
        ],
        'last_name' => [
            'type' => 'string',
        ],
        'email' => [
            'type' => 'string',
        ],
        'string_password' => [
            'type' => 'string',
            'label' => 'Password',
            'show_view' => false,
        ],
    ];

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    protected $columns = ['first_name', 'last_name', 'email'];

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    protected $searchable = ['name', 'email'];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->first_name));
    }
}
