<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravelcrud\Crud\Crud;
use function PHPSTORM_META\type;

class News extends Model
{
    use Crud;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'text', 'active', 'activate_at', 'image',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    public $fields = [
        'title' => [
            'type' => 'string',
        ],
        'description' => [
            'type' => 'text',
        ],
        'text' => [
            'type' => 'inline',
        ],
        'active' => [
            'type' => 'bool',
        ],
        'activate_at' => [
            'type' => 'date',
            'format' => 'YYYY-MM-DD HH:mm:ss'
        ],
        'image' => [
            'label' => 'Image',
            'type' => 'file',
            'aspect_ratio' => 1,
            'show_mutator' => 'getImage'
        ],
        'list_of_categories' => [
            'label' => 'Categories',
            'type' => 'multiselect',
            'selector' => 'getCategories',
            'show_relation' => 'categories',
        ]
    ];

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    protected $columns = ['title', 'description'];

    /**
     * The attributes for CRUD methods
     *
     * @var array
     */
    protected $searchable = ['title', 'description'];

    /**
     * The relation to categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {
        return $this->belongsToMany(Category::class, 'new_category', 'new_id');
    }

    /**
     * Get categories list
     *
     * @return mixed
     */
    public function getCategories() {
        return Category::get()->map(function ($object){
            return [
                'label' => $object->title,
                'value' => $object->id,
            ];
        });
    }

    /**
     * The selected categories
     *
     * @return mixed
     */
    public function getListOfCategoriesAttribute() {
        return $this->categories->map(function ($object){
            return $object->id;
        })->toArray();
    }

    public function getImage()
    {
        return '<img width="200" class="img" src="'. asset('storage/' . $this->image).'">';
    }
}
