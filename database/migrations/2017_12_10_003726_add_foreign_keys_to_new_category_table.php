<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNewCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('new_category', function(Blueprint $table)
		{
			$table->foreign('category_id', 'new_category_categories_FK')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('new_id', 'new_category_news_FK')->references('id')->on('news')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('new_category', function(Blueprint $table)
		{
			$table->dropForeign('new_category_categories_FK');
			$table->dropForeign('new_category_news_FK');
		});
	}

}
