<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->string('description', 1000);
			$table->text('text', 65535);
			$table->boolean('active')->nullable()->default(0);
			$table->dateTime('activate_at')->nullable();
			$table->string('image')->nullable();
			$table->timestamps();
			$table->integer('user_id')->nullable()->index('news_user_id_IDX');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
