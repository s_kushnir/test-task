<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'admin@admin.com',
            'password' =>  bcrypt('1234567'),
            'first_name' => 'Jones',
            'last_name' => 'Roy',
        ]);
        $this->command->info('Created the sample user.');
    }
}
