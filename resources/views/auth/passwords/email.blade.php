@extends('adminlte::passwords.email')

@section('css')
    <link rel="stylesheet" href="{{asset('css/preloader.css')}}">
@endsection

@section('body')
    @parent

    <div id="overlay">
        <div id="progstat">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
            </div>
        </div>
        <div id="progress"></div>
    </div>
@endsection

@section('js')
    <script src="{{asset('js/preloader.js')}}"></script>
@endsection
