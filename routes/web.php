<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as' => 'admin.',
    'middleware' => 'auth',
    'namespace' => 'Admin'
], function(){
    Route::get('', 'HomeController@index')->name('home');

    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');
    Route::resource('news', 'NewController');
});

Auth::routes();

