<?php
/**
 * Created at 12/4/17 4:53 PM by Serhiy K
 * Copyright (c) 2017, Unified AV Ltd. All rights reserved.
 */

return [
/*
   |--------------------------------------------------------------------------
   | Menu Items
   |--------------------------------------------------------------------------
   |
   | Specify your menu items to display in the left sidebar. Each menu item
   | should have a text and and a URL. You can also specify an icon from
   | Font Awesome. A string instead of an array represents a header in sidebar
   | layout. The 'can' is a filter on Laravel's built in Gate functionality.
   |
   */
    'menu' => [
        [
            'text' => 'News',
            'route'  => 'admin.news.index',
            'icon' => 'newspaper-o',
        ],
        [
            'text' => 'Categories',
            'route'  => 'admin.categories.index',
            'icon' => 'thumb-tack',
        ],
        [
            'text' => 'Users',
            'route'  => 'admin.users.index',
            'icon' => 'users',
        ],

    ],
    'prefix' => 'admin',
];